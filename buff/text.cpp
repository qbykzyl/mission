#include "opencv2/opencv.hpp"
#include <vector>
#include <iostream>

using namespace std;
using namespace cv;
Vec3f fitCircle(const std::vector<cv::Point2f> &points)
{
    if (points.size() >= 3)
    {
        cv::Mat A(points.size(), 3, CV_32FC1);
        cv::Mat B(points.size(), 1, CV_32FC1);

        for (size_t i = 0; i < points.size(); ++i)
        {
            float x = points[i].x;
            float y = points[i].y;

            A.at<float>(i, 0) = 2 * x;
            A.at<float>(i, 1) = 2 * y;
            A.at<float>(i, 2) = 1;

            B.at<float>(i, 0) = -(x * x + y * y);
        }
        cv::Mat solution;
        cv::solve(A, B, solution, cv::DECOMP_SVD);

        float center_x = -solution.at<float>(0, 0);
        float center_y = -solution.at<float>(1, 0);
        float radius = sqrt(center_x * center_x + center_y * center_y - solution.at<float>(2, 0));
        return Vec3f(center_x, center_y, radius);
    }
    else return Vec3f(7,7,7);
}
int main(int argc, char **argv)
{

    cv ::VideoCapture capture("../buff.mp4");

    if (!capture.isOpened())
    {
        std ::cout << "无法读取视频" << std ::endl;
        return 0;
    }
    cv ::Mat frame;
    vector<cv::Point2f> points;
    RotatedRect rect;
    while (true)
    {
        capture.read(frame);

        if (frame.empty())
        {
            capture.open("../buff.mp4");
            std ::cout << "open video" << std ::endl;
            capture.read(frame);
           
        }
        ///////////////////////
        //Image preprocessing//
        ///////////////////////
        vector<Mat> imgChannels;
        split(frame, imgChannels);
        Mat midImage = imgChannels.at(2) - imgChannels.at(0);
        threshold(midImage, midImage, 100, 255, THRESH_BINARY);
        int structElementSize = 2;
        Mat element = getStructuringElement(MORPH_RECT,
                                            Size(2 * structElementSize + 1, 2 * structElementSize + 1),
                                            Point(structElementSize, structElementSize));
        dilate(midImage, midImage, element);
        structElementSize = 3;
        element = getStructuringElement(MORPH_RECT,
                                        Size(2 * structElementSize + 1, 2 * structElementSize + 1),
                                        Point(structElementSize, structElementSize));
        morphologyEx(midImage, midImage, MORPH_CLOSE, element);
        vector<vector<Point>> contours;
        vector<Vec4i> hierarchy;
        findContours(midImage, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
        ///////////////////////
        // Features matching //
        ///////////////////////
        if (!contours.empty())
        {
            for (int i = 0; i >= 0; i = hierarchy[i][0])
            {
                if (hierarchy[i][2] != -1) // && hierarchy[hierarchy[i][2]][0] == -1&&hierarchy[hierarchy[i][2]][1]==-1)
                {
                    if (hierarchy[hierarchy[i][2]][0] == -1 || contourArea(contours[hierarchy[hierarchy[i][2]][0]]) < 500)
                    {
                        if (hierarchy[hierarchy[i][2]][1] == -1 || contourArea(contours[hierarchy[hierarchy[i][2]][1]]) < 500)
                        {
                            rect = minAreaRect(contours[hierarchy[i][2]]);
                            Point2f p[4];
                            rect.points(p);
                            Point2f center(0, 0);
                            for (int i = 0; i < 4; i++)
                            {
                                center += p[i];
                            }
                            center /= 4;
                            points.push_back(center);
                            Vec3f circl = fitCircle(points);
                            cv::circle(frame, Point(circl[0], circl[1]), circl[2], Scalar(0, 255, 0), 3);
                            cv::circle(frame, center, 5, Scalar(255, 0, 255), -1);
                            cv::putText(frame, "target", center, FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255), 2, LINE_AA);
                            cv::putText(frame, "target", center + Point2f(2, 1), FONT_HERSHEY_SIMPLEX, 1, Scalar(255, 255, 255), 2, LINE_AA);
                            for (int j = 0; j < 4; ++j)
                            {
                                line(frame, p[j], p[(j + 1) % 4], Scalar(255, 255, 255), 2);
                            }
                            break;
                        }
                    }
                }
            }
        }
        namedWindow("mid", cv::WINDOW_NORMAL);
        resizeWindow("mid", 640, 480);
        imshow("mid", midImage);
        namedWindow("frame", cv::WINDOW_NORMAL);
        resizeWindow("frame", 640, 480);
        imshow("frame", frame);
        waitKey(7);
    }

    return 0;
}
